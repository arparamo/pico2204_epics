/*******************************************************************************
 *
 * Filename: ps2204Block.c
 *
 * Code for tests on epics communication with Picoscope 2204..
 * The code has been simplified from ps2000Con.c by A.R. Paramo
 *
 * To build this application:-
 *
 *    Windows platforms:
 *
 *        If Microsoft Visual Studio (including Express editions) is being used:
 *            Select the solution configuration (Debug/Release) and platform (x86/x64)
 *            Ensure that the 32-/64-bit ps2000.lib can be located
 *            Ensure that the ps2000.h file can be located
 *        
 *        Otherwise:
 *            Set up a project for a 32-/64-bit console mode application
 *            Add this file to the project
 *            Add ps2000.lib to the project (Microsoft C only)
 *            Build the project 
 *
 *  Linux platforms:
 *        Ensure that the libps2000 driver package has been installed using the
 *        instructions from https://www.picotech.com/downloads/linux
 *        Place this file in the same folder as the files from the linux-build-files
 *        folder. In a terminal window, use the following commands to build 
 *        the ps2000Con application:
 *
 *            ./autogen.sh <ENTER>
 *            make <ENTER>
 *
 * Copyright (C) 2006-2018 Pico Technology Ltd. See LICENSE file for terms.
 *
 ******************************************************************************/

#include <stdio.h>

#ifdef WIN32
#include "windows.h"
#include <conio.h>
#include "ps2000.h"
#define PREF4 _stdcall
#else
#include <sys/types.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>

#include "libps2000-2.1/ps2000.h"
#include "cadef.h"

#define PREF4

#define Sleep(a) usleep(1000*a)
#define scanf_s scanf
#define fscanf_s fscanf
#define memcpy_s(a,b,c,d) memcpy(a,c,d)
typedef uint8_t BYTE;
typedef enum enBOOL
{
    FALSE, TRUE
} BOOL;


/* A function to get a single character on Linux */
#define max(a,b) ((a) > (b) ? a : b)
#define min(a,b) ((a) < (b) ? a : b)
#endif

#define BUFFER_SIZE     1000 //1024 //Adq array size.
#define BUFFER_SIZE_STREAMING 50000        // Overview buffer size
#define NUM_STREAMING_SAMPLES 10000000    // Number of streaming samples to collect
#define MAX_CHANNELS 4
#define SINGLE_CH_SCOPE 1 // Single channel scope
#define DUAL_SCOPE 2      // Dual channel scope

// AWG Parameters - 2203, 2204, 2204A, 2205 & 2205A

#define AWG_MAX_BUFFER_SIZE        4096
#define    AWG_DAC_FREQUENCY        2e6
#define AWG_DDS_FREQUENCY        48e6
#define    AWG_PHASE_ACCUMULATOR    4294967296.0

/*******************************************************
* Data types changed:
*
* int8_t   - int8_t
* int16_t  - 16-bit signed integer (int16_t)
* int32_t  - 32-bit signed integer (int)
* uint32_t - 32-bit unsigned integer (unsigned int)
*******************************************************/

int16_t values_a [BUFFER_SIZE]; // block mode buffer, Channel A
int16_t values_b [BUFFER_SIZE]; // block mode buffer, Channel B

int16_t overflow;
int32_t scale_to_mv = 1; 

int16_t channel_mv [PS2000_MAX_CHANNELS];
int16_t g_overflow = 0;

// Streaming data parameters
int16_t g_triggered = 0;
uint32_t g_triggeredAt = 0;
uint32_t g_nValues;
uint32_t g_startIndex;            // Start index in application buffer where data should be written to in streaming mode collection
uint32_t g_prevStartIndex;        // Keep track of previous index into application buffer in streaming mode collection
int16_t  g_appBufferFull = 0;    // Use this in the callback to indicate if it is going to copy past the end of the buffer

typedef enum {
    MODEL_NONE = 0,
    MODEL_PS2204A = 0xA204,
} MODEL_TYPE;

typedef struct{
    PS2000_THRESHOLD_DIRECTION    channelA;
    PS2000_THRESHOLD_DIRECTION    channelB;
    PS2000_THRESHOLD_DIRECTION    ext;
} DIRECTIONS;

typedef struct{
    PS2000_PWQ_CONDITIONS            *    conditions;
    int16_t                            nConditions;
    PS2000_THRESHOLD_DIRECTION        direction;
    uint32_t                        lower;
    uint32_t                        upper;
    PS2000_PULSE_WIDTH_TYPE            type;
} PULSE_WIDTH_QUALIFIER;


typedef struct{
    PS2000_CHANNEL channel;
    float threshold;
    int16_t direction;
    float delay;
} SIMPLE;

typedef struct{
    int16_t hysteresis;
    DIRECTIONS directions;
    int16_t nProperties;
    PS2000_TRIGGER_CONDITIONS * conditions;
    PS2000_TRIGGER_CHANNEL_PROPERTIES * channelProperties;
    PULSE_WIDTH_QUALIFIER pwq;
     uint32_t totalSamples;
    int16_t autoStop;
    int16_t triggered;
} ADVANCED;


typedef struct {
    SIMPLE simple;
    ADVANCED advanced;
} TRIGGER_CHANNEL;

typedef struct {
    int16_t DCcoupled;
    int16_t range;
    int16_t enabled;
    int16_t values [BUFFER_SIZE];
} CHANNEL_SETTINGS;

typedef struct  {
    int16_t            handle;
    MODEL_TYPE        model;
    PS2000_RANGE    firstRange;
    PS2000_RANGE    lastRange;    
    TRIGGER_CHANNEL trigger;
    int16_t            maxTimebase;
    int16_t            timebases;
    int16_t            noOfChannels;
    CHANNEL_SETTINGS channelSettings[PS2000_MAX_CHANNELS];
    int16_t            hasAdvancedTriggering;
    int16_t            hasFastStreaming;
    int16_t            hasEts;
    int16_t            hasSignalGenerator;
    int16_t            awgBufferSize;
} UNIT_MODEL; 

// Struct to help with retrieving data into 
// application buffers in streaming data capture
typedef struct
{
    UNIT_MODEL unit;
    int16_t *appBuffers[DUAL_SCOPE * 2];
    uint32_t bufferSizes[PS2000_MAX_CHANNELS];
} BUFFER_INFO;

UNIT_MODEL unitOpened;
int32_t times[BUFFER_SIZE];
int32_t input_ranges [PS2000_MAX_RANGES] = {10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000};

/****************************************************************************
 * adc_to_mv
 *
 * If the user selects scaling to millivolts,
 * Convert an 12-bit ADC count into millivolts
 ****************************************************************************/
int32_t adc_to_mv (int32_t raw, int32_t Vrange){
    return ( scale_to_mv ) ? ( raw * input_ranges[Vrange] ) / 32767 : raw; 
}

/****************************************************************************
 * mv_to_adc
 *
 * Convert a millivolt value into a 12-bit ADC count
 *
 *  (useful for setting trigger thresholds)
 ****************************************************************************/
int16_t mv_to_adc (int16_t mv, int16_t Vrange){
    return ( ( mv * 32767 ) / input_ranges[Vrange] );
}

/****************************************************************************
 * set_defaults - restore default settings
 ****************************************************************************/
void set_defaults (void){
    int16_t ch = 0;
    ps2000_set_ets ( unitOpened.handle, PS2000_ETS_OFF, 0, 0 );

    for (ch = 0; ch < unitOpened.noOfChannels; ch++)    {
        ps2000_set_channel ( unitOpened.handle,
                               ch,
                                unitOpened.channelSettings[ch].enabled ,
                                unitOpened.channelSettings[ch].DCcoupled ,
                                unitOpened.channelSettings[ch].range);
    }//for
}

/****************************************************************************
 * Collect_block_triggered
 *  this function demonstrates how to collect a single block of data from the
 *  unit, when a trigger event occurs.
 ****************************************************************************/

void collect_block_triggered (void){
    int32_t i;
    int32_t trigger_sample;
    int32_t time_interval;
    int16_t time_units;
    int16_t oversample=1;
    int32_t time_indisposed_ms;
    int16_t overflow;
    int32_t max_samples;
    int16_t ch;

    int16_t trigDelay;
    int16_t threshold_mV = 2500;
    int16_t threshold_adc;

    int16_t timebase=5;
    int16_t auto_trigger_ms = 2000;
    int16_t v_rangeA=9, v_rangeB=99;
    int32_t voltageA, voltageB;


    ///////////////
    //EPICS INITIALIZE
    ///////////////

    printf ( "Create EPICs PV channels\n" );

    // Voltage Range PV
    dbr_enum_t update_PV_val = 1;
    char update_PV_str[] = "picoscope:Update";
    chid    mychid_update;

    ca_create_channel( update_PV_str, NULL,NULL, 10, &mychid_update ) ;
    ca_pend_io(5.0) ;

    ca_put( DBR_ENUM, mychid_update, &update_PV_val  );
    ca_pend_io(5.0) ;


    // Trigger Settings
    dbr_long_t trigThr_PV_val = -1;
    char trigThr_PV_str[] = "picoscope:trigThr";
    chid    mychid_trigThr;
    ca_create_channel( trigThr_PV_str, NULL,NULL, 10, &mychid_trigThr ) ;
    ca_pend_io(5.0) ;

    dbr_long_t trigDelay_PV_val = -1;
    char trigDelay_PV_str[] = "picoscope:trigDelay";
    chid    mychid_trigDelay;
    ca_create_channel( trigDelay_PV_str, NULL,NULL, 10, &mychid_trigDelay ) ;
    ca_pend_io(5.0) ;

    // Voltage Range PV
    dbr_enum_t Vrange_PV_val = -1;
    char Vrange_PV_str[] = "picoscope:Vrange";
    chid    mychid_Vrange;

    ca_create_channel( Vrange_PV_str, NULL,NULL, 10, &mychid_Vrange ) ;
    ca_pend_io(5.0) ;
    
    // Timebase PV
    dbr_enum_t timebase_PV_val = -1;
    char timebase_PV_str[] = "picoscope:ts";
    chid    mychid_ts;
    ca_create_channel( timebase_PV_str, NULL,NULL, 10, &mychid_ts ) ;
    ca_pend_io(5.0) ;

    // TIME
    unsigned nelm;
    unsigned nBytes;
    chid    mychid_t;
    dbr_long_t *t_PV_val;
    char t_PV_str[] = "picoscope:T";

    ca_create_channel( t_PV_str, NULL,NULL,10,&mychid_t) ;
    ca_pend_io(5.0) ;
    nelm = ca_element_count ( mychid_t );
    nBytes = dbr_size_n ( DBR_LONG, nelm );
    t_PV_val = ( dbr_long_t * ) malloc ( nBytes ); // Memory Alocation

    // WFM 
    dbr_long_t *wfm_PV_val;
    chid mychid_wfm;
    char wfm_PV_str[] = "picoscope:WFM";

    ca_create_channel( wfm_PV_str, NULL,NULL,10,&mychid_wfm) ;
    ca_pend_io(1.0) ;
    nelm = ca_element_count ( mychid_wfm );
    nBytes = dbr_size_n ( DBR_LONG, nelm );
    wfm_PV_val = ( dbr_long_t * ) malloc ( nBytes ); // Memory Alocation

    ///////////////
    //Set Voltage Range
    ///////////////
   
    void update_voltageRange(){
        ca_get( DBR_ENUM, mychid_Vrange, &Vrange_PV_val ) ;
        ca_pend_io(5.0) ;
        v_rangeA = (int16_t) Vrange_PV_val;
        
        unitOpened.channelSettings[0].range = v_rangeA;
        voltageA = input_ranges[unitOpened.channelSettings[0].range];

        unitOpened.channelSettings[1].range = v_rangeB;
        voltageB = input_ranges[unitOpened.channelSettings[1].range];

        if (v_rangeA == 99){
            unitOpened.channelSettings[0].enabled = FALSE;
        }else{ unitOpened.channelSettings[0].enabled = TRUE;}

        if (v_rangeB == 99){
            unitOpened.channelSettings[1].enabled = FALSE;
        }else{ unitOpened.channelSettings[1].enabled = TRUE;}

        printf("\nCh A. Range %d; Voltage: %d mV\n", v_rangeA, voltageA   );
        printf("Ch B. Range %d; Voltage: %d mV\n", v_rangeB, voltageB   );


        ///////////////
        // SET TRIGGER
        ///////////////
        //
        ca_get( DBR_LONG, mychid_trigThr, &trigThr_PV_val ) ;
        ca_pend_io(5.0) ;
        threshold_mV = (int16_t) trigThr_PV_val;
        threshold_adc = mv_to_adc( threshold_mV, v_rangeA );

        ca_get( DBR_LONG, mychid_trigDelay, &trigDelay_PV_val ) ;
        ca_pend_io(5.0) ;
        trigDelay = (int16_t) trigDelay_PV_val;


        printf ( "Collects when value rises past %d mV; %d adc\n", threshold_mV, threshold_adc );
        /* Trigger enabled
         * ChannelA - to trigger unsing this channel it needs to be enabled using ps2000_set_channel()
        * Rising edge
        * Threshold = 100mV
        * 10% pre-trigger  (negative is pre-, positive is post-)
        */
        unitOpened.trigger.simple.channel = PS2000_CHANNEL_A;
        unitOpened.trigger.simple.direction = (int16_t) PS2000_RISING;
        unitOpened.trigger.simple.threshold = threshold_adc;
        unitOpened.trigger.simple.delay = trigDelay;

        ps2000_set_trigger ( unitOpened.handle,
                                (int16_t) unitOpened.trigger.simple.channel,
                                (int16_t) threshold_adc,
                                (int16_t) unitOpened.trigger.simple.direction,
                                (int16_t) unitOpened.trigger.simple.delay,
                                (int16_t) auto_trigger_ms ); 
        //ps2000_set_trigger ( unitOpened.handle, PS2000_NONE, 0, PS2000_RISING, 0, auto_trigger_ms ); // For no trigger.
        
        set_defaults(); //Necessary to update voltage settings
    };

    ///////////////
    //Get timebase from pv
    ///////////////

    void update_timebase(){
        ca_get( DBR_ENUM, mychid_ts, &timebase_PV_val ) ;
        ca_pend_io(5.0) ;
        timebase = (int16_t)timebase_PV_val ;
        
        //Set & Get timebase. Timebase. Voltage set needs to be before.
        ps2000_get_timebase ( unitOpened.handle, timebase, BUFFER_SIZE , &time_interval, &time_units, oversample, &max_samples );
        printf ( "\n Timebase %d; dt %d ns; Samples %d Total Pulse  %.2E s\n", timebase, time_interval, BUFFER_SIZE, (float)time_interval*1E-9*BUFFER_SIZE  );

        // Initialize Time
        for (i=0; i<nelm; i++){
            t_PV_val[i] = (dbr_long_t)( time_interval*i ) ;
        };//for

        ca_array_put(DBR_LONG,nelm, mychid_t, t_PV_val) ;
        ca_pend_io(1.0) ;
    }


    while(1){

        ///////////////
        //UPDATE SCOPE SETTINGS
        ///////////////
        ca_get( DBR_ENUM, mychid_update, &update_PV_val ) ;
        ca_pend_io(5.0) ;
        if ( update_PV_val == 1){

            printf ( "\n Update Settings \n"  );
            update_timebase();
            update_voltageRange();

            update_PV_val = 0;
            ca_put( DBR_ENUM, mychid_update, &update_PV_val  );
            ca_pend_io(5.0) ;
        }//if


        ///////////////////
        // START SAMPLING
        ///////////////////
        
        ps2000_run_block ( unitOpened.handle, BUFFER_SIZE, timebase, oversample, &time_indisposed_ms );
        printf ( "Waiting for trigger...\n" );
        while ( !ps2000_ready ( unitOpened.handle ) ) {
            Sleep ( 5 );
        }//while
        ps2000_stop ( unitOpened.handle );

        // Get the times (in units specified by time_units) and the values (in ADC counts)
        ps2000_get_times_and_values ( unitOpened.handle,
                                            times,
                                            unitOpened.channelSettings[PS2000_CHANNEL_A].values,
                                            unitOpened.channelSettings[PS2000_CHANNEL_B].values,
                                            NULL,
                                            NULL,
                                            &overflow, time_units, BUFFER_SIZE );
        printf ( "Pulse Obtained\n\n" );

        ///////////////
        //EPICS COMMNUNICATION
        ///////////////
        for (i=0; i<nelm; i++){
            //wfm_PV_val[i] = (dbr_long_t)  unitOpened.channelSettings[0].values[i] ; //ADC Counts
            wfm_PV_val[i] = (dbr_long_t) adc_to_mv(  unitOpened.channelSettings[0].values[i], v_rangeA ) ; // mV
        };//for
        ca_array_put(DBR_LONG,nelm, mychid_wfm, wfm_PV_val);
        ca_pend_io(1.0);

    }//while

}//collect_block

/****************************************************************************
 *
 *
 ****************************************************************************/
void get_info (void){
    int8_t description [8][25]=  {    "Driver Version   ",
                                    "USB Version      ",
                                    "Hardware Version ",
                                    "Variant Info     ",
                                    "Serial           ", 
                                    "Cal Date         ", 
                                    "Error Code       ",
                                    "Kernel Driver    "
                                    };
    int16_t     i;
    int8_t        line [80];
    int32_t        variant;

    if( unitOpened.handle )    {
        for ( i = 0; i < 8; i++ ){
            ps2000_get_unit_info ( unitOpened.handle, line, sizeof (line), i );
        
            if (i == 3){
                variant = atoi((const char*) line);
                if (strlen((const char*) line) == 5){ // Identify if 2204A or 2205A
                    line[4] = toupper(line[4]);
                    if (line[1] == '2' && line[4] == 'A'){        // i.e 2204A -> 0xA204
                        variant += 0x9968;
                    }//if
                }//if
            }//if
        
            if(i != 6){ // No need to print error code
                printf ( "%s: %s\n", description[i], line );
            }//if
        }//for

        switch (variant){
            case MODEL_PS2204A:
                    unitOpened.model = MODEL_PS2204A;
                    unitOpened.firstRange = PS2000_50MV;
                    unitOpened.lastRange = PS2000_20V;
                    unitOpened.maxTimebase = PS2200_MAX_TIMEBASE;
                    unitOpened.timebases = unitOpened.maxTimebase;
                    unitOpened.noOfChannels = DUAL_SCOPE;
                    unitOpened.hasAdvancedTriggering = TRUE;
                    unitOpened.hasSignalGenerator = TRUE;
                    unitOpened.hasEts = TRUE;
                    unitOpened.hasFastStreaming = TRUE;
                    unitOpened.awgBufferSize = 4096;
            break;
             default:
                printf("Unit not supported");
        }//switch
        
        unitOpened.channelSettings [PS2000_CHANNEL_A].enabled = 1;
        unitOpened.channelSettings [PS2000_CHANNEL_A].DCcoupled = 1;
        unitOpened.channelSettings [PS2000_CHANNEL_A].range = PS2000_5V;

        if (unitOpened.noOfChannels == DUAL_SCOPE){
            unitOpened.channelSettings [PS2000_CHANNEL_B].enabled = 1;
        }else{
            unitOpened.channelSettings [PS2000_CHANNEL_B].enabled = 0;
        }//if

        unitOpened.channelSettings [PS2000_CHANNEL_B].DCcoupled = 1;
        unitOpened.channelSettings [PS2000_CHANNEL_B].range = PS2000_5V;

        set_defaults();
    }
    else{
        printf ( "Unit Not Opened\n" );
    
        ps2000_get_unit_info ( unitOpened.handle, line, sizeof (line), 5 );
    
        printf ( "%s: %s\n", description[5], line );
        unitOpened.model = MODEL_NONE;
        unitOpened.firstRange = PS2000_100MV;
        unitOpened.lastRange = PS2000_20V;
        unitOpened.timebases = PS2105_MAX_TIMEBASE;
        unitOpened.noOfChannels = SINGLE_CH_SCOPE;    
    }//if
}

/****************************************************************************
 *
 *
 ****************************************************************************/

void main (void)
{
    int8_t    ch;

    printf ( "PicoScope 2000 Series (ps2000) Driver Example Program\n" );
    printf ( "Version 1.3\n\n" );
    printf ( "\n\nOpening the device...\n");

    //open unit and show splash screen
    unitOpened.handle = ps2000_open_unit ();
    printf ( "Handler: %d\n", unitOpened.handle );

    if ( !unitOpened.handle ){
        printf ( "Unable to open device\n" );
        exit ( 99 );
    }else{
        printf ( "Device opened successfully\n\n" );
        get_info ();

        collect_block_triggered ();
        ps2000_close_unit ( unitOpened.handle );
    }
}
