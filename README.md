# Picoscope 2204 Acquisition in EPICs


## Index  
[Introduction](#Introduction)  <br>
[Installation](#Installation) <br>
[Other Notes](#Other Notes) <br>


<a name="Introduction"/><br>
## Introduction

This repository includes code for epics communication with Picoscope 2204.

The source file is ps2204.c that includes the c program for Picoscope acquisition and publishing into EPICS.
The EPICs IOC is run through a softIOC.
The signal for the Picoscope is generated with a mega arduino run through serial.

The code has been executed in a RaspberryPi.

<figure>
<img src="tests/setup.jpg" alt="alt text" width="250">
  <figcaption><b>Set Up for with the Raspi, Mega and Picoscope 2024.</b></figcaption>
</figure>

As an example it is possible to observe 10 us or 100 us pulses:

<figure>
<img src="tests/Pulse_10us.png" alt="alt text" width="300">
<img src="tests/Pulse_100us.png" alt="alt text" width="300">
  <figcaption><b>10 us and 100 us us Pulse.</b></figcaption>
</figure>

<a name="Installation"/><br>
## Installation

 * Picoscope libraries
      *  Follow Instructions https://www.picotech.com/downloads/linux
      *  `sudo bash -c 'echo deb https://labs.picotech.com/debian/ picoscope main >/etc/apt/sources.list.d/picoscope.list'`
      *  `wget -O - https://labs.picotech.com/debian/dists/picoscope/Release.gpg.key | sudo apt-key add -`
      *  `sudo apt-get update`
      *  `sudo apt-get install libps2000`
 * Picoscope Examples
   * https://github.com/picotech/picosdk-c-examples.git
   * Text files need to be converted to linux with `dos2unit`
 *  Install EPICs base 
    * https://epics.anl.gov/base/R3-16/2.php
    * Configure EPICS in bashrc:<br>
`export EPICS_ROOT=/usr/local/epics`<br>
`export EPICS_BASE=${EPICS_ROOT}/base`<br>
`export EPICS_HOST_ARCH=${EPICS_BASE}/startup/EpicsHostArch`<br>
`export EPICS_BASE_BIN=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}`<br>
`export EPICS_BASE_LIB=${EPICS_BASE}/lib/${EPICS_HOST_ARCH}`<br>
`if [ "" = "${LD_LIBRARY_PATH}" ]; then`<br>
`    export LD_LIBRARY_PATH=${EPICS_BASE_LIB}`<br>
`else`<br>
`    export LD_LIBRARY_PATH=${EPICS_BASE_LIB}:${LD_LIBRARY_PATH}`<br>
`fi`<br>
`export PATH=${PATH}:${EPICS_BASE_BIN}`<br>
`export EPICS_CA_AUTO_ADDR_LIST=NO #As desired`<br>
`export EPICS_CA_ADDR_LIST=192.168.12.3`<br>

* Modify configure.ac to include EPICs libraries
    *  `CFLAGS="${CFLAGS} -I$pico_headers_path -I/usr/local/epics/base/include/ -I/usr/local/epics/base/include/os/Linux/ -I/usr/local/epics/base/include/compiler/gcc/\
 -L${EPICS_BASE_LIB} -lca"`
* Run ./autogen.sh then make
* Run softIOC `softIOC -d softIOC.db`
* Run pulse generator `python pySerial.py`
* Run Picoscope program `./ps2204`

<a name="Other Notes"/><br>
## Other Notes
 * For remote display in `export DISPLAY=localhost.0.0` in bashrc
