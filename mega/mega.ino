// Pins deffinition
const int ledPin = 2;

// Read variables
char lastRecvd;
String input = ""; 
char *pch;

//Program Variables
int ledState = 0;

// Pulse variables
unsigned long pulse_t1;

void setup() {

    pinMode(ledPin, OUTPUT);
    Serial.begin(9600); // Make sure the Serial commands match this baud rate

 }

void loop () {
 delay(100);
 while (Serial.available()>0){
    lastRecvd = Serial.read();
    
    if (lastRecvd == '\n'){
        switch(input[0]){
             case 'P':
                //Writes P 10000000 
                //         Pulse duration // Time in us              
               
                pch = strtok (input.c_str() ," "); //reads P

                pch = strtok (NULL ," "); //
                pulse_t1 =  atol( pch ) ;

                if (pulse_t1 < 1000){
                    digitalWrite(ledPin, 1);
                    delayMicroseconds(pulse_t1);
                    digitalWrite(ledPin, 0);
                } else {
                    digitalWrite(ledPin, 1);
                    delay(pulse_t1/1000);
                    digitalWrite(ledPin, 0);                
                }
                
                
                      
                Serial.println();
                input = ""; 

                break; 
            default:
               input = ""; 
               break;}//switch
           
     }else { // Input is still coming in
        input += lastRecvd;}

  }//while
}//loop
